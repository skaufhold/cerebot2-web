import { createExpect } from 'expect-async';
import { Configuration, ChatUser } from 'cerebot2-common';
import request from 'supertest'
import mongoose from 'mongoose';

describe('configuration controller', function () {
    beforeEach(() => {
        this.adminUser = new ChatUser({nick: 'testuser', admin: true});
        this.inactiveConfig = {_meta: {name: 'Test inactive', active: false}};
        this.activeConfig = {_meta: {name: 'Test active', active: true}};
    });

    it('creates a configuration', (done) => {
        var expect = createExpect(done);
        passportStub.login(this.adminUser);

        let config = new Configuration(this.activeConfig).toObject();
        delete config._id;

        request(app)
            .put('/configurations')
            .set('Accept', 'application/json')
            .send(config)
            .expect(200)
            .expect('Content-Type', /json/)
            .end((err, res) => {
                if (err) return done(err);
                let createdConfig = new Configuration(res.body);
                expect(createdConfig.get('_meta.name')).toEqual('Test active');
                Configuration.findById(createdConfig._id).then(c => {
                    expect(c.get('_meta.name')).toEqual('Test active');
                    expect(c.get('_meta.active')).toEqual(true);
                    done();
                });
            });
    });

    it('updates a configuration', (done) => {
        var expect = createExpect(done);
        passportStub.login(this.adminUser);

        Configuration.create(this.inactiveConfig).then((configuration) => {
            configuration.set('_meta.name', 'Test changed');
            request(app)
                .put('/configurations')
                .set('Accept', 'application/json')
                .send(configuration.toObject())
                .expect(200)
                .expect('Content-Type', /json/)
                .end((err, res) => {
                    if (err) return done(err);
                    let updatedConfig = new Configuration(res.body);
                    expect(updatedConfig.get('_meta.name')).toEqual('Test changed');
                    Configuration.findById(updatedConfig._id).then(c => {
                        expect(c.get('_meta.name')).toEqual('Test changed');
                        expect(c.get('_meta.active')).toEqual(false);
                        done();
                    });
                });
        });
    });

    describe('update attribute', () => {
        it('successfully', (done) => {
            var expect = createExpect(done);
            passportStub.login(this.adminUser);

            Configuration.create(this.inactiveConfig).then((configuration) => {
                request(app)
                    .post('/configurations/' + configuration._id)
                    .set('Accept', 'application/json')
                    .send({
                        property: '_meta.name',
                        value: 'Test 2'
                    })
                    .expect('Content-Type', /json/)
                    .expect(200)
                    .end((err, res) => {
                        if (err) return done(err);
                        let updatedConfig = new Configuration(res.body);
                        expect(updatedConfig.get('_meta.name')).toEqual('Test 2');
                        done();
                    });
            });
        });

        it('fails with non-existent attribute', (done) => {
            var expect = createExpect(done);
            passportStub.login(this.adminUser);

            Configuration.create(this.inactiveConfig).then((configuration) => {
                request(app)
                    .post('/configurations/' + configuration._id)
                    .set('Accept', 'application/json')
                    .send({
                        property: '_meta.name.lalala',
                        value: 'Test 2'
                    })
                    .expect(400, done);
            });
        });

        it('fails unless authenticated', (done) => {
            var expect = createExpect(done);
            Configuration.create(this.inactiveConfig).then((configuration) => {
                request(app)
                    .post('/configurations/' + configuration._id)
                    .set('Accept', 'application/json')
                    .send({
                        property: '_meta.name',
                        value: 'Test 2'
                    })
                    .expect(401, done);
            });
        });
    });

    it('gets active configuration', (done) => {
        var expect = createExpect(done);
        passportStub.login(this.adminUser);

        Configuration.create([this.inactiveConfig, this.activeConfig]).then((configs) => {
            request(app).get('/configurations/active')
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200)
                .end((err, res) => {
                    if (err) return done(err);
                    const activeConfig = new Configuration(res.body);
                    expect(activeConfig._meta.active).toBe(true);
                    done();
                });
        });
    });

    it('gets list of configurations', (done) => {
        var expect = createExpect(done);
        passportStub.login(this.adminUser);

        Configuration.create([this.inactiveConfig, this.activeConfig]).then((configs) => {
            request(app).get('/configurations')
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200)
                .end((err, res) => {
                    if (err) return done(err);
                    const loadedConfigs = res.body.map(c => new Configuration(c)._id);
                    const expectedConfigs = configs.sort((a,b) => a._meta.name.localeCompare(b._meta.name)).map(c => c._id);
                    expect(loadedConfigs).toEqual(expectedConfigs);
                    done();
                });
        });
    });

    describe('select active configuration', () => {
        it('successful', (done) => {
            var expect = createExpect(done);
            passportStub.login(this.adminUser);

            Configuration.create([this.inactiveConfig, this.inactiveConfig, this.activeConfig]).then((configs) => {
                request(app).post('/configurations/active')
                    .set('Accept', 'application/json')
                    .send({id: configs[0]._id})
                    .expect(204)
                    .end((err) => {
                        if (err) return done(err);
                        Configuration.find({'_meta.active': true}).then(activeConfigs => {
                            expect(activeConfigs.length).toBe(1);
                            expect(activeConfigs[0]._id).toEqual(configs[0]._id);
                            done();
                        }, done);
                    });
            })
        });

        it('fails with no id', (done) => {
            passportStub.login(this.adminUser);

            Configuration.create(this.inactiveConfig).then((configs) => {
                request(app).post('/configurations/active')
                    .set('Accept', 'application/json')
                    .expect(400, done);
            })
        });

        it('fails with invalid id', (done) => {
            passportStub.login(this.adminUser);

            request(app).post('/configurations/active')
                .set('Accept', 'application/json')
                .send({id: mongoose.Types.ObjectId()})
                .expect(404, done);
        });
    });

    describe('delete', () => {
        it('successfully', (done) => {
            var expect = createExpect(done);
            passportStub.login(this.adminUser);

            Configuration.create(this.inactiveConfig).then(config => {
                request(app).delete('/configurations/' + config._id)
                    .set('Accept', 'application/json')
                    .expect(204)
                    .end(err => {
                        if (err) return done(err);
                        Configuration.findById(config._id).then(c => {
                            expect(c).toBe(null);
                            done();
                        }, done);
                    });
            })
        });

        it('error when not found', (done) => {
            passportStub.login(this.adminUser);
            request(app).delete('/configurations/' + mongoose.Types.ObjectId())
                .set('Accept', 'application/json')
                .expect(404, done);
        });

        it('error if config is active', (done) => {
            var expect = createExpect(done);
            passportStub.login(this.adminUser);

            Configuration.create(this.activeConfig).then(config => {
                request(app).delete('/configurations/' + config._id)
                    .set('Accept', 'application/json')
                    .expect(422)
                    .end(err => {
                        if (err) return done(err);
                        Configuration.findById(config._id).then(c => {
                            expect(c._id).toEqual(config._id);
                            done();
                        }, done);
                    });
            })
        });
    });
});