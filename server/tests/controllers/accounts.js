import expect from 'expect';
import { ChatUser } from 'cerebot2-common';
import request from 'supertest';

describe('local login strategy', function() {
    before(function()  {
        passportStub.uninstall();
    });

    after(function()  {
        passportStub.install(app);
    });

    it('logs a valid user in', (done) => {
        ChatUser.create({
            account: {
                email: "test@example.org",
                password: "password123"
            }
        }).then(() => {
            request(app)
                .post('/login')
                .set('Accept', 'application/json')
                .send({email: "test@example.org", password: "password123"})
                .expect(200)
                .end((err, res) => {
                    let gotUser = res.body;
                    expect(gotUser.account.email).toEqual("test@example.org");
                    done();
                });
        }, done);
    });

    it('fails with wrong password', (done) => {
        ChatUser.create({
            account: {
                email: "test@example.org",
                password: "password123"
            }
        }).then(() => {
            request(app)
                .post('/login')
                .set('Accept', 'application/json')
                .send({email: "test@example.org", password: "password321"})
                .expect(401)
                .end((err, res) => {
                    expect(res.body.error.message).toEqual('Invalid email or password');
                    done();
                });
        }, done);
    });
});