import expect from 'expect';
import { Configuration, ChatUser } from 'cerebot2-common';
import request from 'supertest'

describe('chatUser controller', function () {
    before(() => {
        this.modUser = new ChatUser({nick: 'testmod', moderator: true});
        this.normalUser = new ChatUser({nick: 'testuser'});
    });

    it('lists users', (done) => {
        passportStub.login(this.modUser);

        ChatUser.create([this.normalUser, this.modUser]).then((users) => {
            request(app)
                .get('/users')
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200)
                .end((err, res) => {
                    if (err) return done(err);
                    const loadedUsers = res.body.docs.map(c => new ChatUser(c));
                    expect(loadedUsers.map(c => c._id)).toEqual(users.map(c => c._id));
                    done();
                });
        });
    });

    it('sorts the user list', (done) => {
        passportStub.login(this.modUser);

        const sortableUsers = [new ChatUser({nick: 'atestuser'}), new ChatUser({nick: 'ctestuser'}), new ChatUser({nick: 'btestuser'})];

        ChatUser.create(sortableUsers).then((users) => {
            users.sort((a, b) => a.nick.localeCompare(b.nick));
            request(app)
                .get('/users')
                .query({sort: 'nick;asc'})
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200)
                .end((err, res) => {
                    if (err) return done(err);
                    const loadedUsers = res.body.docs.map(c => new ChatUser(c));
                    expect(loadedUsers.map(c => c._id)).toEqual(users.map(c => c._id));
                    done();
                });
        });
    });
});