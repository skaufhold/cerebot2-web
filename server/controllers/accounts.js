'use strict';

var ChatUser = require('cerebot2-common').ChatUser;
var passport = require('passport');

/**
 * POST /login
 */
exports.postLogin = function (req, res, next) {
    // Do email and password validation for the server
    passport.authenticate('local', (err, user, info) => {
        if (err) return next(err);
        if (!user) {
            let notFoundErr = new Error(info.message);
            notFoundErr.status = 401;
            // this is horrible, so hacky
            res.json({
                error: {
                    name: notFoundErr.name,
                    message: notFoundErr.message,
                    text: notFoundErr.toString()
                }
            });
            return next(err);
        }
        req.logIn(user, function (err) {
            if (err) return next(err);
            res.format({
                json: () => {
                    res.json(user);
                },
                html: () => {
                    req.flash('success', {msg: 'Success! You are logged in'});
                    res.end('Success');
                }
            });
        });
    })(req, res, next);
};


/**
 * GET /logout
 */
exports.getLogout = function (req, res, next) {
    // Do email and password validation for the server
    req.logout();
    res.format({
        json: () => {
            res.end();
        },
        html: () => {
            next();
        }
    });
};

/**
 * POST /signup
 * Create a new local account
 */
exports.postSignUp = function (req, res, next) {
    var user = new ChatUser({
        account: {
            email: req.body.email,
            password: req.body.password
        }
    });

    ChatUser.findOne({'account.email': req.body.email}, function (err, existingUser) {
        if (existingUser) {
            req.flash('errors', {msg: 'Account with that email address already exists'});
        }
        user.save(function (err) {
            if (err) return next(err);
            req.logIn(user, function (err) {
                if (err) return next(err);
                res.format({
                    json: () => {
                        res.json(user);
                    },
                    html: () => {
                        req.flash('success', {msg: 'Success!'});
                        res.end('Success');
                    }
                });
            });
        });
    });
};
