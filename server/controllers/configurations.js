"use strict";

var common = require('cerebot2-common');
var Configuration = common.Configuration;

function configurationNotFound(next) {
    let err = new Error('Configuration not found');
    err.status = 404;
    return next(err);
}

function idParamMissing(next) {
    let err = new Error('Id Parameter missing');
    err.status = 400;
    return next(err);
}

exports.index = function(req, res, next) {
    res.format({
        html: () => next(),
        json: () => {
            Configuration.find({}).sort({'_meta.name': 'asc'}).then(
                configurations => {
                    res.json(configurations.map(c => c.toObject()));
                },
                next
            );
        }
    });
};

exports.findActive = function(req, res, next) {
    res.format({
        html: () => next(),
        json: () => {
            Configuration.findOne({'_meta.active': true}).then(
                configuration => {
                    res.json(configuration);
                },
                next
            );
        }
    });
};

exports.selectActive = function(req, res, next) {
    if (!req.body.id) {
        return idParamMissing(next);
    }
    Configuration.findOne({_id: req.body.id}).then(
        (newConfig) => {
            if (!newConfig) {
                return configurationNotFound(next);
            }
            Configuration.update({'_meta.active': true}, {'_meta.active': false}).then(
                () => {
                    newConfig._meta.active = true;
                    newConfig.save().then(
                        () => {
                            res.sendStatus(204);
                            res.end();
                        }, next
                    )
                }, next
            );
        }, next
    );
};

exports.delete = function(req, res, next) {
    if (!req.params.id) {
        return idParamMissing(next);
    }
    Configuration.findById(req.params.id).then(
        (config) => {
            if (!config) {
                return configurationNotFound(next);
            }
            if (config._meta.active) {
                let err = new Error("Can't delete active configuration");
                err.status = 422;
                return next(err);
            }
            config.remove().then(() => {
                res.sendStatus(204);
                res.end();
            }, next);
        }, next);
};

exports.put = function(req, res, next) {
    if (req.body._id) {
        Configuration.findById(req.body._id).then((config) => {
            if (!config) {
                return configurationNotFound(next);
            }

            let content = Object.assign({}, req.body);
            delete content._id;
            delete content.__v;

            config.set(content);
            config.save().then(updatedConfig => {
                res.json(updatedConfig);
                res.end();
            }, next);
        }, next)
    } else {
        Configuration.create(req.body).then((config) => {
            res.json(config);
            res.end();
        }, next);
    }
};

exports.updateProperty = function(req, res, next) {
    if (!req.params.id) {
        return idParamMissing(next);
    }

    Configuration.findOne({_id: req.params.id}).then(
        (config) => {
            if (!config) {
                return configurationNotFound(next);
            }
            else {
                const pathIsValid = Object.getOwnPropertyNames(config.schema.paths).indexOf(req.body.property) >= 0;
                if (pathIsValid) {
                    let newValue = req.body.value;
                    if (config.schema.paths[req.body.property].instance == 'Array') {
                        newValue = req.body.value.split(/, ?/);
                    }

                    config.set(req.body.property, newValue);
                    config.save().then(
                        (c) => {
                            res.json(c);
                            res.end();
                        },
                        next
                    );
                } else {
                    res.sendStatus(400);
                    res.end();
                }
            }
        },
        next
    );
};