"use strict";

var common = require('cerebot2-common');
var ChatUser = common.ChatUser,
    Message = common.Message;

/**
 * GET /users
 */
exports.index = function(req, res, next) {
    res.format({
        html: () => next(),
        json: () => {
            let sort = {};
            if (req.query.sort) {
                const sortAttr = req.query.sort.split(";");
                const pathNames = Object.getOwnPropertyNames(ChatUser.schema.paths)
                if (pathNames.indexOf(sortAttr[0]) >= 0 &&
                    (sortAttr[1] == 'asc' || sortAttr[1] == 'desc')) {
                    sort[sortAttr[0]] = sortAttr[1];
                }
            }

            ChatUser.paginate({}, {
                offset: req.query.offset,
                limit: req.query.limit,
                sort,
                lean: true
            }).then(
                users => {
                    res.json(users);
                },
                next
            );
        }
    });
};

exports.findOne = function(req, res, next) {
    res.format({
        html: () => next(),
        json: () => {
            ChatUser.findOne({nick: req.params.nick}).then(
                user => {
                    res.json(user);
                },
                next
            );
        }
    });
};

exports.history = function(req, res, next) {
    res.format({
        html: () => next(),
        json: () => {
            Message.paginate({nick: req.params.nick}, {
                offset: req.query.offset,
                limit: req.query.limit,
                sort: { _id: 'desc' },
                lean: true
            }).then(
                result => {
                    res.json(result);
                },
                next
            );
        }
    });
};