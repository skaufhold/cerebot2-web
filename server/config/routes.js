/**
 * Routes for express app
 */
var express = require('express');
var accounts = require('../controllers/accounts');
var chatUsers = require('../controllers/chatUsers');
var configurations = require('../controllers/configurations');
var mongoose = require('mongoose');
var auth = require('../utils/auth.js');
var App = require('../../public/assets/app.server').default;

module.exports = function (app, passport) {
    // user routes
    app.post('/login', accounts.postLogin);
    app.post('/signup', accounts.postSignUp);
    app.get('/logout', accounts.getLogout);

    app.get('/users', auth.isMod, chatUsers.index);
    app.get('/users/:nick', auth.isMod, chatUsers.findOne);
    app.get('/users/:nick/history', auth.isMod, chatUsers.history);

    app.get('/configurations', auth.isMod, configurations.index);
    app.get('/configurations/active', auth.isMod, configurations.findActive);
    app.post('/configurations/active', auth.isAdmin, configurations.selectActive);
    app.post('/configurations/:id', auth.isAdmin, configurations.updateProperty);
    app.delete('/configurations/:id', auth.isAdmin, configurations.delete);
    app.put('/configurations', auth.isAdmin, configurations.put);

    app.get("/auth/twitch", passport.authenticate("twitch"));
    app.get("/auth/twitch/callback",
        passport.authenticate("twitch", {
            successRedirect: '/',
            successFlash: 'Logged in successfully!',
            failWithError: true
        }));

    // This is where the magic happens. We take the locals data we have already
    // fetched and seed our stores with data.
    // App is a function that requires store data and url to initialize and return the React-rendered html string
    app.get('*', function (req, res, next) {
        console.log('render app');
        App(req, res);
    });

};
