module.exports = {
    db: 'mongodb://localhost/CerebotTest',
    sessionSecret: 'testsessionsecret',
    twitch: {
        clientID: '',
        clientSecret: '',
        callbackURL: 'http://localhost:3000/auth/twitch/callback'
    }
};
