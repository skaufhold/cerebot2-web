
module.exports = {
    // Find the appropriate database to connect to, default to localhost if not found.
    db: process.env.MONGOHQ_URL || process.env.MONGOLAB_URI || 'mongodb://localhost/Cerebot',
    sessionSecret: process.env.SESSION_SECRET || 'Your Session Secret goes here',
    twitch: {
        clientID: process.env.TWITCH_CLIENTID || "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
        clientSecret: process.env.TWITCH_SECRET || "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
        callbackURL: process.env.TWITCH_CALLBACK || "http://localhost:3000/auth/twitch/callback"
    }
};
