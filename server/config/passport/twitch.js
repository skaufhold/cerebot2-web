"use strict";

var TwitchStrategy = require('passport-twitch').Strategy;
var cerebotCommon = require('cerebot2-common');
var ChatUser = cerebotCommon.ChatUser;
var secrets = require('../secrets');
var camelize = require('underscore.string').camelize;

module.exports = new TwitchStrategy(Object.assign(secrets.twitch, { scope: "user_read" }),
    function(accessToken, refreshToken, profile, done) {
        let twitchProfile = profile._json;
        twitchProfile.updated_at = new Date(twitchProfile.updated_at);
        twitchProfile.created_at = new Date(twitchProfile.updated_at);

        let normalizedTwitchProfile = {};

        for (const propName in twitchProfile) {
            if (!twitchProfile.hasOwnProperty(propName)) continue;
            normalizedTwitchProfile[camelize(propName.toLowerCase())] = twitchProfile[propName];
        }

        ChatUser.findOne({nick: normalizedTwitchProfile.name}).then((user) => {
            if (user) {
                user.account = user.account || {};
                user.account.twitch = normalizedTwitchProfile;
                user.save(done);
            }
            else {
                new ChatUser({
                    nick: normalizedTwitchProfile.name,
                    account: {
                        twitch: normalizedTwitchProfile
                    }
                }).save(done);
            }
        }, err => done(err, null));
    }
);
