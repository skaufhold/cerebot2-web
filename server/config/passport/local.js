/*
 Configuring local strategy to authenticate strategies
 Code modified from : https://github.com/madhums/node-express-mongoose-demo/blob/master/config/passport/local.js
 */

var mongoose = require('mongoose');
var LocalStrategy = require('passport-local').Strategy;
var ChatUser = require('cerebot2-common').ChatUser;

/*
 By default, LocalStrategy expects to find credentials in parameters named username and password.
 If your site prefers to name these fields differently, options are available to change the defaults.
 */
module.exports = new LocalStrategy({
  usernameField : 'email'
}, function(email, password, done) {
  ChatUser.findOne({ 'account.email': email }, function(err, user) {
    if(!user) return done(null, false, { message: 'Invalid email or password'});
    user.account.comparePassword(password, function(err, isMatch) {
      if(isMatch) {
        return done(null, user);
      } else {
        return done(null, false, { message: 'Invalid email or password'});
      }
    });
  });
});