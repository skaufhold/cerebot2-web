/* Initializing passport.js */
var ChatUser = require('cerebot2-common').ChatUser;
var local = require('./passport/local');
var twitch = require('./passport/twitch');

/*
 * Expose
 */
module.exports = function(app, passport, config) {
    // serialize sessions
    passport.serializeUser(function(user, done) {
        done(null, user._id);
    });

    passport.deserializeUser(function(id, done) {
        ChatUser.findOne({_id: id}, function(err, user) {
            done(err, user);
        });
    });

    //use the following strategies
    passport.use(local);
    passport.use(twitch);
};