module.exports = {
    db: process.env.MONGO_URL,
    sessionSecret: process.env.SESSION_SECRET,
    twitch: {
        clientID: process.env.TWITCH_CLIENTID,
        clientSecret: process.env.TWITCH_SECRET,
        callbackURL: process.env.TWITCH_CALLBACK
    }
};
