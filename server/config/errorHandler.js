"use strict";

module.exports = function (app, passport) {
    // Middleware error handler for json response
    app.use(function handleError(err, req, res, next) {
        if (err) {
            var statusCode = err.status || 500;
            res.sendStatus(statusCode);
            res.format({
                html: () => {
                    req.flash('error', err.message);
                    res.redirect('/');
                },
                json: () => {
                    res.json({
                        error: {
                            name: err.name,
                            message: err.message,
                            text: err.toString()
                        }
                    });
                }
            });
        }
        else {
            next();
        }
    });
};