require('dotenv').config();

var isTest = process.env.NODE_ENV === 'test';
var isDev = process.env.NODE_ENV === 'development';

var express = require('express');
var fs = require('fs');

var passport = require('passport');
var webpack = require('webpack');

var secrets = isTest ? require('./config/secrets.test') : require('./config/secrets');
var config = require('../webpack/dev.browser.js');

var app = exports.app = express();
var compiler = webpack(config);

var common = exports.common = require('cerebot2-common');
var mongoose = common.mongoose;
common.connect(secrets.db);

mongoose.connection.on('error', console.log);
mongoose.connection.on('disconnected', common.connect);

if (isDev) {
    app.use(require('webpack-dev-middleware')(compiler, {
        noInfo: true,
        publicPath: config.output.publicPath
    }));

    app.use(require('webpack-hot-middleware')(compiler, {
        log: console.log, path: '/__webpack_hmr', heartbeat: 10 * 1000
    }));
}


// Bootstrap passport config
require('./config/passport')(app, passport);

// Bootstrap application settings
require('./config/express')(app, passport);

// Bootstrap routes
require('./config/routes')(app, passport);

require('./config/errorHandler')(app, passport);

app.listen((!isTest) ? app.get('port') : 3001);
