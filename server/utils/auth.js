
const noAccessError = new Error("Sorry, you're not allowed to see this!");
noAccessError.status = 401;

module.exports = {
    isAuthenticated: function (req, res, next) {
        if (req.isAuthenticated())
            return next();
        next(noAccessError);
    },
    isAdmin: function (req, res, next) {
        if (req.isAuthenticated() && req.user.admin)
            return next();
        next(noAccessError);
    },
    isMod: function (req, res, next) {
        if (req.isAuthenticated() && (req.user.admin || req.user.moderator))
            return next();
        next(noAccessError);
    }
};
