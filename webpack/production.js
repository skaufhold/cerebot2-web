var path = require("path");
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var webpack = require("webpack");

var assetsPath = path.join(__dirname, "..", "public", "assets");
var publicPath = "assets/";

var commonLoaders = require('./commonLoaders');

module.exports = [
    {
        name: "browser",
        devtool: "source-map",
        context: path.join(__dirname, "..", "app"),
        entry: {
            app: "./client"
        },
        output: {
            path: assetsPath,
            filename: "[name].js",
            publicPath: publicPath
        },
        module: {
            preLoaders: [{
                test: /\.js$|\.jsx$/,
                exclude: /node_modules/,
                loaders: ["eslint"]
            }],
            loaders: commonLoaders.concat([{
                test: /\.(scss|css)$/,
                loader: ExtractTextPlugin.extract('style-loader', 'css-loader?localIdentName=[local]__[hash:base64:5]!autoprefixer-loader!sass?includePaths[]='
                    + encodeURIComponent(path.resolve(__dirname, '..', 'app', 'scss')))
            }])
        },
        resolve: {
            extensions: ['', '.js', '.jsx', '.scss', '.css'],
            modulesDirectories: [
                "app", "node_modules"
            ]
        },
        plugins: [
            new webpack.DefinePlugin({
                'process.env': {
                    'NODE_ENV': JSON.stringify('production')
                }
            }),
            new webpack.optimize.OccurenceOrderPlugin(),
            new ExtractTextPlugin("styles/main.css"),
            new webpack.optimize.UglifyJsPlugin({
                compressor: {
                    warnings: false
                }
            })
        ]
    }, {
        // The configuration for the server-side rendering
        name: "server-side rendering",
        context: path.join(__dirname, "..", "app"),
        entry: {
            app: "./server"
        },
        target: "node",
        output: {
            // The output directory as absolute path
            path: assetsPath,
            // The filename of the entry chunk as relative path inside the output.path directory
            filename: "[name].server.js",
            // The output path from the view of the Javascript
            publicPath: publicPath,
            libraryTarget: "commonjs2"
        },
        module: {
            loaders: commonLoaders.concat([{
                test: /\.(scss|css)$/,
                loader: ExtractTextPlugin.extract('style-loader', 'css-loader?localIdentName=[local]__[hash:base64:5]!autoprefixer-loader!sass?includePaths[]='
                    + encodeURIComponent(path.resolve(__dirname, '..', 'app', 'scss')))
            }])
        },
        resolve: {
            extensions: ['', '.js', '.jsx', '.scss', '.css'],
            modulesDirectories: [
                "app", "node_modules"
            ]
        },
        plugins: [
            // Order the modules and chunks by occurrence.
            // This saves space, because often referenced modules
            // and chunks get smaller ids.
            new webpack.optimize.OccurenceOrderPlugin(),
            // extract inline css from modules into separate files
            new ExtractTextPlugin("styles/main.css"),
            new webpack.optimize.UglifyJsPlugin({
                compressor: {
                    warnings: false
                }
            })
        ]
    }
];
