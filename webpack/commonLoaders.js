var path = require("path");

module.exports = [
    {
        test: /\.js$|\.jsx$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
        query: {
            presets: ['es2015', 'react']
        },
        include: path.join(__dirname, "..", "app")
    },
    {test: /\.(woff|woff2)$/, loader: "url-loader?limit=10000&minetype=application/font-woff"},
    {test: /\.png$/, loader: 'url-loader'},
    {test: /\.(jpg|ttf|eot|svg|json)$/, loader: 'file-loader'},
    {test: /\.html$/, loader: 'html-loader'}
];
