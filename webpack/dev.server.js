var path = require("path");
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var webpack = require("webpack");

var assetsPath = path.join(__dirname, "..", "public", "assets");
var publicPath = "assets/";

var commonLoaders = require('./commonLoaders');

module.exports = {
    name: "server-side rendering",
    context: path.join(__dirname, "..", "app"),
    entry: {
        app: "./server"
    },
    target: "node",
    output: {
        path: assetsPath,
        filename: "[name].server.js",
        publicPath: publicPath,
        libraryTarget: "commonjs2"
    },
    module: {
        loaders: commonLoaders.concat([{
            test: /\.(scss|css)$/,
            loader: ExtractTextPlugin.extract('style-loader', 'css-loader?localIdentName=[local]__[hash:base64:5]!autoprefixer-loader!sass?includePaths[]='
                + encodeURIComponent(path.resolve(__dirname, '..', 'app', 'scss')))
        }])
    },
    resolve: {
        extensions: ['', '.js', '.jsx', '.scss', '.css'],
        modulesDirectories: [
            "app", "node_modules"
        ]
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new webpack.optimize.OccurenceOrderPlugin(),
        new ExtractTextPlugin("styles/main.css")
    ]
};
