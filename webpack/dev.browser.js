var path = require('path');
var webpack = require('webpack');
var assetsPath = path.join(__dirname, '..', 'public', 'assets');
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var hotMiddlewareScript = 'webpack-hot-middleware/client?path=/__webpack_hmr&timeout=20000&reload=true';

var commonLoaders = require('./commonLoaders');

module.exports = {
    devtool: 'inline-source-map',
    // The configuration for the client
    name: 'browser',
    context: path.join(__dirname, '..', 'app'),
    entry: {
        app: ['./client', hotMiddlewareScript]
    },
    output: {
        path: assetsPath,
        filename: '[name].js',
        publicPath: '/assets/'
    },
    module: {
        loaders: commonLoaders.concat([
            {
                test: /\.(scss|css)$/,
                loader: ExtractTextPlugin.extract('style-loader', 'css-loader?localIdentName=[local]__[hash:base64:5]!autoprefixer-loader!sass?includePaths[]='
                    + encodeURIComponent(path.resolve(__dirname, '..', 'app', 'scss')))
            }
        ])
    },
    resolve: {
        extensions: ['', '.js', '.jsx', '.scss'],
        modulesDirectories: [
            'app', 'node_modules'
        ]
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NoErrorsPlugin(),
        new ExtractTextPlugin("styles/main.css")
    ]
};
