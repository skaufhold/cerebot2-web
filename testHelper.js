
import sinon from 'sinon';
import sinonMocha from 'sinon-mocha';
sinonMocha.enhance(sinon);
import passportStub from 'passport-stub';
import { ChatUser } from 'cerebot2-common';

import { app, common } from './server/index';
const mongoose = common.mongoose;

global.app = app;
global.passportStub = passportStub;
global.baseUrl = 'http://localhost:3000';

function clearCollections() {
    return new Promise((resolve, reject) => {
        mongoose.connection.db.collections(function(err, collections) {
            const promises = collections.map((coll) => {
                new Promise((done, fail) => {
                    if (coll.s.name.startsWith('system')) {
                        return done();
                    } else {
                        coll.remove(() => {
                            done();
                        });
                    }
                });
            });
            Promise.all(promises).then(() => resolve(), reject)
        });
    });
}

before(function(done) {
    passportStub.install(app);

    if (mongoose.connection.readyState != 1) {
        mongoose.connection.on('connected', () => {
            clearCollections().then(done);
        });
    } else {
        clearCollections().then(done);
    }
});

beforeEach(function(done)  {
    passportStub.logout();
    clearCollections().then(done);
});

after(function(done)  {
    passportStub.uninstall();
    clearCollections().then(done);
});