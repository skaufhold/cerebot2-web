import React from 'react';
import { Route } from 'react-router';

import App from 'components/App';
import Home from 'components/Home';
import Logout from './components/Logout';
import Login from './components/Login';
import ConfigurationPage from 'components/ConfigurationPage';
import ChatUserIndex from 'components/chatUser/ChatUserIndex';
import ChatUserPage from 'components/chatUser/UserPage';

import { requireAuthentication } from 'components/authenticateComponent';

export default (
    <Route component={App}>
        <Route path="/" component={Home}/>
        <Route path="logout" component={Logout}/>
        <Route path="login" component={Login}/>
        <Route path="users" component={requireAuthentication(ChatUserIndex, 'moderator')}/>
        <Route path="users/:nick" component={requireAuthentication(ChatUserPage, 'moderator')}/>
        <Route path="configurations" component={requireAuthentication(ConfigurationPage, 'admin')}/>
    </Route>
);
