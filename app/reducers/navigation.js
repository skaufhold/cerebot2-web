import {
    NAVIGATION_SELECT_ITEM
} from '../constants/actionTypes';

export default function settings(state={
    navItem: null
}, action={}) {
    switch (action.type) {
        case NAVIGATION_SELECT_ITEM:
            return Object.assign({}, state, {
                navItem: action.navItem
            });
        default:
            return state;
    }
}
