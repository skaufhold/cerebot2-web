import {
    SHOW_FLASH,
    NAVIGATION_SELECT_ITEM
} from '../constants/actionTypes';

export default function settings(state={
    success: [],
    error: [],
    info: []
}, action={}) {
    switch (action.type) {
        case SHOW_FLASH:
            return Object.assign({}, state, {
                success: (action.success) ? state.success.concat(action.success) : state.success,
                error: (action.error) ? state.error.concat(action.error) : state.error,
                info: (action.info) ? state.info.concat(action.info) : state.info
            });
        case NAVIGATION_SELECT_ITEM:
            return Object.assign({}, state, {
                success: [],
                error: [],
                info: []
            });
        default:
            return state;
    }
}
