import {
    CONFIGURATIONS_LOAD,
    CONFIGURATIONS_LOAD_DONE,
    CONFIGURATIONS_LOAD_ERROR,
    CONFIGURATION_EDIT_BEGIN,
    CONFIGURATION_EDIT_CANCEL,
    CONFIGURATION_EDIT_SAVE,
    CONFIGURATION_EDIT_SAVE_DONE,
    LOGOUT_SUCCESS_USER,
    CONFIGURATION_SET_ACTIVE_BEGIN,
    CONFIGURATION_SET_ACTIVE_DONE,
    CONFIGURATION_SET_ACTIVE_ERROR,
    CONFIGURATION_CREATE_BEGIN,
    CONFIGURATION_CREATE_ERROR,
    CONFIGURATION_CREATE_DONE,
    CONFIGURATION_DELETE_DONE,
    CONFIGURATION_DELETE_ERROR
} from '../constants/actionTypes';

export default function settings(state={
    isWaiting: false,
    available: [],
    isEditing: false,
    editingPropertyName: null,
    editingConfiguration: null,
    isSaving: false
}, action={}) {
    switch (action.type) {
        case CONFIGURATIONS_LOAD:
            return Object.assign({}, state, {
                isWaiting: true
            });
        case CONFIGURATIONS_LOAD_DONE:
            const activeConfiguration = action.configurations.find(c => c._meta.active);
            return Object.assign({}, state, {
                isWaiting: false,
                available: action.configurations,
                active: activeConfiguration,
                editingConfiguration: activeConfiguration
            });
        case CONFIGURATIONS_LOAD_ERROR:
            return Object.assign({}, state, {
                isWaiting: false
            });
        case CONFIGURATION_EDIT_BEGIN:
            return Object.assign({}, state, {
                isEditing: true,
                editingPropertyName: action.propertyName
            });
        case CONFIGURATION_EDIT_CANCEL:
            return Object.assign({}, state, {
                isEditing: false,
                editingPropertyName: null
            });
        case CONFIGURATION_EDIT_SAVE:
            return Object.assign({}, state, {
                isSaving: true
            });
        case CONFIGURATION_EDIT_SAVE_DONE:
            return Object.assign({}, state, {
                isEditing: false,
                isSaving: false,
                editingPropertyName: null,
                editingConfiguration: action.configuration
            });
        case CONFIGURATION_SET_ACTIVE_DONE:
            const configurations = state.available.map(c => {
                c._meta.active = (c._id == action.configurationId);
                return c;
            });
            return Object.assign({}, state, {
                available: configurations,
                editingConfiguration: configurations.find(c => c._meta.active)
            });
        case CONFIGURATION_CREATE_DONE:
            return Object.assign({}, state, {
                available: state.available.concat([action.configuration])
            });
        case CONFIGURATION_DELETE_DONE:
            return Object.assign({}, state, {
                available: state.available.filter(c => c._id != action.configurationId)
            });
        case LOGOUT_SUCCESS_USER:
            return Object.assign({}, state, {
                isWaiting: false,
                available: [],
                editingPropertyName: null,
                editingConfiguration: null
            });
        default:
            return state;
    }
}
