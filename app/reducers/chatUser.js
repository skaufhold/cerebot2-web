import {
    USER_INDEX_LOAD,
    USER_INDEX_LOAD_DONE,
    USER_INDEX_LOAD_ERROR,
    USER_SHOW_LOAD,
    USER_SHOW_LOAD_DONE,
    USER_SHOW_LOAD_ERROR,
    USER_HISTORY_LOAD,
    USER_HISTORY_LOAD_DONE,
    USER_HISTORY_LOAD_ERROR,
    LOGOUT_SUCCESS_USER
} from '../constants/actionTypes';

export default function chatUser(state={
    isWaiting: false,
    users: null,
    sort: {},
    user: null,
    history: null
}, action={}) {
    switch (action.type) {
        case USER_INDEX_LOAD:
            return Object.assign({}, state, {
                isWaiting: true,
                users: null,
                sort: action.sort
            });
        case USER_INDEX_LOAD_DONE:
            return Object.assign({}, state, {
                isWaiting: false,
                users: action.users
            });
        case USER_INDEX_LOAD_ERROR:
            return Object.assign({}, state, {
                isWaiting: false
            });
        case USER_SHOW_LOAD:
            return Object.assign({}, state, {
                isWaiting: true,
                user: null
            });
        case USER_SHOW_LOAD_DONE:
            return Object.assign({}, state, {
                isWaiting: false,
                user: action.user
            });
        case USER_SHOW_LOAD_ERROR:
            return Object.assign({}, state, {
                isWaiting: false
            });
        case USER_HISTORY_LOAD:
            return Object.assign({}, state, {
                isWaiting: true,
                history: null
            });
        case USER_HISTORY_LOAD_DONE:
            return Object.assign({}, state, {
                isWaiting: false,
                history: action.history
            });
        case USER_HISTORY_LOAD_ERROR:
            return Object.assign({}, state, {
                isWaiting: false
            });
        case LOGOUT_SUCCESS_USER:
            return Object.assign({}, state, {
                user: null,
                history: null,
                users: null
            });
        default:
            return state;
    }
}
