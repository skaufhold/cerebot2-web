import { combineReducers } from 'redux';
import account from 'reducers/account';
import chatUser from 'reducers/chatUser';
import configuration from 'reducers/configuration';
import navigation from 'reducers/navigation';
import flash from 'reducers/flash';
import { routeReducer as routing } from 'react-router-redux';

// Combine reducers with routeReducer which keeps track of
// router state
const rootReducer = combineReducers({
    routing,
    account,
    chatUser,
    configuration,
    navigation,
    flash
});

export default rootReducer;
