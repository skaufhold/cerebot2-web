import React, { Component, PropTypes } from 'react';
import Alert from 'react-bootstrap/lib/Alert';
import Collapse from 'react-bootstrap/lib/Collapse';
import { connect } from 'react-redux';

const flashKeyToBootstrap = {
    error: 'danger',
    success: 'success',
    info: 'info'
};

class Flash extends Component {
    constructor(...args) {
        super(...args);

        this.state = {
            dismissed: []
        };
    }

    render() {
        return (<div>
            { Object.getOwnPropertyNames(this.props.flash).map((type) => {
                if (this.props.flash[type].length > 0) {
                    if (type !== 'error') {
                        return (
                            <Collapse key={type} in={this.state.dismissed.indexOf(type) < 0}>
                                <Alert bsStyle={flashKeyToBootstrap[type]} dismissAfter={5000}
                                       onDismiss={()=>this.handleDismiss(type)}>
                                    {this.props.flash[type].map((message, index) => <p key={index}>{message}</p>)}
                                </Alert>
                            </Collapse>);
                    }

                    return (
                        <Collapse key={type} in={this.state.dismissed.indexOf(type) < 0}>
                            <Alert bsStyle={flashKeyToBootstrap[type]}>
                                {this.props.flash[type].map((message, index) => <p key={index}
                                                                                   onDismiss={()=>this.handleDismiss(type)}>{message}</p>)}
                            </Alert>
                        </Collapse>);
                    }
                })}
        </div>);
    }

    handleDismiss(type) {
        this.setState({dismissed: this.state.dismissed.concat([type])});
    }
}

Flash.propTypes = {
    flash: PropTypes.object
};

function mapStateToProps(state) {
    return {
        flash: state.flash
    };
}

export default connect(mapStateToProps)(Flash);
