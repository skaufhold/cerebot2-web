import SpinkitSpinner from 'react-spinkit';
import '../scss/components/_spinner.scss';

export default class Spinner extends SpinkitSpinner {
    constructor(props) {
        super(Object.assign({
            spinnerName: 'three-bounce'
        }, props));
    }
}
