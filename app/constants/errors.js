
export const unknownError = {
    name: 'UnknownError',
    message: 'Something bad happened!',
    text: 'Something bad happened!'
};
