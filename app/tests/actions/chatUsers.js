import expect from 'expect';
import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import nock from 'nock';
import * as actions from '../../actions/chatUsers';
import * as types from '../../constants/actionTypes';

const middlewares = [ thunk ];
const mockStore = configureStore(middlewares);

describe('chatUsers actions', () => {
    afterEach(() => {
        nock.cleanAll();
    });

    it('creates USER_SHOW_LOAD_DONE after loading user', (done) => {
        const user = {
            "_id": "aaaaaaaaaaaaaaaa",
            "nick": "test",
            "__v": 14,
            "messageCount": 1,
            "lastSeen": "2016-01-14T19:04:18.413Z"
        };

        nock(baseUrl).get(`/users/${user.nick}`)
            .reply(200, user);

        const expectedActions = [
            { type: types.USER_SHOW_LOAD },
            { type: types.USER_SHOW_LOAD_DONE, user }
        ];

        const store = mockStore({ user: [] }, expectedActions, done);
        store.dispatch(actions.loadUser("test"));
    });

    it('creates USER_SHOW_LOAD_ERROR on error', (done) => {
        nock(baseUrl).get(`/users/test`)
            .reply(500);

        const expectedActions = [
            { type: types.USER_SHOW_LOAD },
            { type: types.USER_SHOW_LOAD_ERROR }
        ];

        const store = mockStore({ user: [] }, expectedActions, done);
        store.dispatch(actions.loadUser("test"));
    });

    it('creates USER_INDEX_LOAD_DONE after loading userlist with sorting', (done) => {
        const userlist = {
            docs: [{
                "_id": "aaaaaaaaaaaaaaaa",
                "nick": "test",
                "__v": 14,
                "messageCount": 1,
                "lastSeen": "2016-01-14T19:04:18.413Z"
            }]
        };

        nock(baseUrl).get("/users")
            .query({
                sort: 'messageCount;asc;',
                offset: 0,
                limit: 30
            })
            .reply(200, userlist);

        const expectedActions = [
            { type: types.USER_INDEX_LOAD, sort: { messageCount: 'asc' } },
            { type: types.USER_INDEX_LOAD_DONE, users: userlist }
        ];

        const store = mockStore({ users: {} }, expectedActions, done);
        store.dispatch(actions.loadUserList({ messageCount: 'asc' }, 0, 30));
    });
});