import expect from 'expect';
import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import nock from 'nock';
import qs from 'qs';
import * as actions from '../../actions/configurations';
import * as types from '../../constants/actionTypes';

const middlewares = [ thunk ];
const mockStore = configureStore(middlewares);

nock.emitter.on('no match', function(req) {
    if (req) console.warn("No match on request: ", req.path);
});

describe('configuration actions', () => {
    afterEach(() => {
        nock.cleanAll();
    });

    it('creates CONFIGURATIONS_LOAD_DONE after loading', (done) => {
        const configurations = [
            {
                "_id": "568c6a719f0eebe522e2f0fe",
                "_meta": {
                    "name": "Default",
                    "active": true
                },
                "__v": 0
            }];

        nock(baseUrl).get('/configurations')
            .reply(200, configurations);

        const expectedActions = [
            { type: types.CONFIGURATIONS_LOAD },
            { type: types.CONFIGURATIONS_LOAD_DONE, configurations: configurations }
        ];

        const store = mockStore({ configurations: [] }, expectedActions, done);
        store.dispatch(actions.loadConfigurations());
    });

    it ('creates CONFIGURATIONS_LOAD_ERROR if loading error occurs',  (done) => {
        nock(baseUrl).get('/configurations')
            .reply(500, "");

        const expectedActions = [
            { type: types.CONFIGURATIONS_LOAD },
            { type: types.CONFIGURATIONS_LOAD_ERROR }
        ];

        const store = mockStore({ configurations: [] }, expectedActions, done);
        store.dispatch(actions.loadConfigurations());
    });

    it ('creates CONFIGURATION_EDIT_SAVE_DONE after updating a configuration',  (done) => {
        const configuration = {
            "_id": "testId",
            "_meta": {
                "name": "Test",
                "active": true
            },
            "__v": 0
        };
        nock(baseUrl).post('/configurations/testId', qs.stringify({
            property: '_meta.name',
            value: 'Test 2'
        })).reply(200, configuration);

        const expectedActions = [
            { type: types.CONFIGURATION_EDIT_SAVE },
            { type: types.CONFIGURATION_EDIT_SAVE_DONE, configuration }
        ];

        const store = mockStore({ configurations: [] }, expectedActions, done);
        store.dispatch(actions.updateProperty('testId', '_meta.name', 'Test 2'));
    });

    it ('creates CONFIGURATION_EDIT_SAVE_ERROR after updating a configuration fails',  (done) => {
        const configuration = {
            "_id": "568c6a719f0eebe522e2f0fe",
            "_meta": {
                "name": "Test",
                "active": true
            },
            "__v": 0
        };
        const error = {
            name: 'Error',
            message: 'Something bad'
        };

        nock(baseUrl).post('/configurations/testId', qs.stringify({
            property: '_meta.name',
            value: 'Test'
        })).reply(500, {error});

        const expectedActions = [
            { type: types.CONFIGURATION_EDIT_SAVE },
            { type: types.CONFIGURATION_EDIT_SAVE_ERROR, error }
        ];

        const store = mockStore({ configurations: [] }, expectedActions, done);
        store.dispatch(actions.updateProperty('testId', '_meta.name', 'Test'));
    });
});
