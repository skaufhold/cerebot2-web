import { createStore, compose, applyMiddleware } from 'redux';
import rootReducer from 'reducers';
import thunk from 'redux-thunk';
import createLogger from 'redux-logger';

const enhancer = applyMiddleware(thunk);

export default function configureStore(initialState) {
    return createStore(rootReducer, initialState, enhancer);
}
