import { createStore, compose, applyMiddleware } from 'redux';
import rootReducer from 'reducers';
import thunk from 'redux-thunk';
import createLogger from 'redux-logger';

const enhancer = compose(
    applyMiddleware(thunk),
    typeof window === 'object' && window.devToolsExtension ? window.devToolsExtension() : applyMiddleware(createLogger())
);

export default function configureStore(initialState) {
    const store = createStore(rootReducer, initialState, enhancer);

    if (module.hot) {
        // Enable Webpack hot module replacement for reducers
        module.hot.accept('reducers', () => {
            const nextReducer = require('reducers');
            store.replaceReducer(nextReducer);
        });
    }

    return store;
}
