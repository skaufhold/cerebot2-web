import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

import Well from 'react-bootstrap/lib/Well';
import Button from 'react-bootstrap/lib/Button';
import Col from 'react-bootstrap/lib/Col';

export default class Login extends Component {
    render() {
        return (
            <Col xs={12} md={8} mdOffset={2} lg={6} lgOffset={3}>
                <Well bsSize="large">
                    <h3>Log in with your Twitch account</h3>
                    <a className="login__button" href="/auth/twitch"><Button block bsStyle="primary">Connect</Button></a>
                </Well>
            </Col>
        );
    }
}

Login.propTypes = {
    next: PropTypes.string,
    authenticated: PropTypes.bool
};

function mapStateToProps(state) {
    return {
        authenticated: state.account.authenticated
    };
}

export default connect(mapStateToProps)(Login);
