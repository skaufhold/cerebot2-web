import React, { Component, PropTypes } from 'react';
import BsPanel from 'react-bootstrap/lib/Panel';
import BsListGroup from 'react-bootstrap/lib/ListGroup';

import humanize from 'underscore.string/humanize';
import titleize from 'underscore.string/titleize';

import ConfigurationEditorProperty from './ConfigurationEditorProperty';

export default class ConfigurationEditorGroup extends Component {
    render() {
        const { propertyGroupName, propertyGroup } = this.props;
        return (
            <BsPanel header={propertyGroup._groupName} className="configurationEditor__group">
                <BsListGroup striped fill>
                    {Object.keys(propertyGroup)
                        .filter(propName => propertyGroup.hasOwnProperty(propName) && !propName.startsWith('_'))
                        .map(propName => {
                        const prop = propertyGroup[propName];
                        return (<ConfigurationEditorProperty key={propName}
                                                            readableName={titleize(humanize(propName))}
                                                            propertyName={propertyGroupName + '.' + propName}
                                                            propertyValue={prop} />);
                        })
                    }
                </BsListGroup>
            </BsPanel>
        );
    }
}

ConfigurationEditorGroup.propTypes = {
    propertyGroup: PropTypes.object.isRequired,
    propertyGroupName: PropTypes.string.isRequired
};
