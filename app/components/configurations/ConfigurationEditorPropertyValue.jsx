import React, { Component, PropTypes } from 'react';
import BsInput from 'react-bootstrap/lib/Input';

export default class ConfigurationEditorPropertyValue extends Component {
    getValue() {
        return (this.input ? this.input.getValue() : this.props.value);
    }

    renderValue() {
        const { value } = this.props;
        switch (typeof (value)) {
            case 'boolean':
                return (value) ? 'Yes' : 'No';
            case 'object':
                if (value instanceof Array) return value.join(', ');
                return value;
            default:
                return value;
        }
    }

    renderInput() {
        const { value } = this.props;
        switch (typeof (value)) {
            case 'string':
                return <BsInput type="text" bsSize="small" defaultValue={value} onChange={this.props.onChange} />;
            case 'boolean':
                return (
                    <BsInput type="select" bsSize="small" defaultValue={value} onChange={this.props.onChange}>
                        <option value={true}>Yes</option>
                        <option value={false}>No</option>
                    </BsInput>
                );
            case 'object':
                if (value instanceof Array) return <BsInput type="text" bsSize="small" defaultValue={value.join(', ')} onChange={this.props.onChange} />;
                return value;
            case 'number':
                return <BsInput type="number" bsSize="small" defaultValue={value} onChange={this.props.onChange} />;
            default:
                return <BsInput type="text" bsSize="small" defaultValue={value} onChange={this.props.onChange} />;
        }
    }

    render() {
        return (<div>{(!this.props.editing ?
            this.renderValue() :
            (this.input = this.renderInput()))}
        </div>);
    }
}
ConfigurationEditorPropertyValue.propTypes = {
    value: PropTypes.any.isRequired,
    name: PropTypes.string.isRequired,
    editing: PropTypes.bool.isRequired,
    readableName: PropTypes.string.isRequired,
    onChange: PropTypes.func
};

ConfigurationEditorPropertyValue.defaultProps = {
    editing: false,
    onChange: () => {}
};
