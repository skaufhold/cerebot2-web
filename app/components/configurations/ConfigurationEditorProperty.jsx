import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import BsListGroupItem from 'react-bootstrap/lib/ListGroupItem';
import BsCol from 'react-bootstrap/lib/Col';
import BsButton from 'react-bootstrap/lib/Button';

import ConfigurationEditorPropertyValue from './ConfigurationEditorPropertyValue';

import {cancelEditProperty, beginEditProperty, updateProperty} from '../../actions/configurations';

class ConfigurationEditorProperty extends Component {
    constructor(props) {
        super(props);
        this.state = {
            propertyValue: props.propertyValue
        };
    }

    render() {
        const { editingPropertyName, readableName, propertyName, propertyValue, dispatch, configuration } = this.props;
        const editing = (editingPropertyName === propertyName);

        return (
            <BsListGroupItem className="configurationEditor__property">
                <BsCol xs={12} sm={4} className="configurationEditor__property__name">{readableName}</BsCol>
                <BsCol xs={9} sm={5} className="configurationEditor__property__value">
                    <ConfigurationEditorPropertyValue name={propertyName} readableName={readableName}
                                                      value={propertyValue} editing={editing}
                                                      onChange={(e) => this.setState({propertyValue: e.currentTarget.value})} />
                </BsCol>
                <BsCol xs={3} sm={3} className="configurationEditor__property__actions">
                    {(!editing ? <BsButton bsSize="xsmall"
                                           onClick={() => dispatch(beginEditProperty(propertyName))}>Edit</BsButton> :
                    <div>
                        <BsButton bsSize="xsmall" onClick={() => dispatch(updateProperty(configuration._id, propertyName, this.state.propertyValue))}>Save</BsButton>
                        <BsButton bsSize="xsmall" onClick={() => dispatch(cancelEditProperty())}>Cancel</BsButton>
                    </div>
                        )}
                </BsCol>
            </BsListGroupItem>
        );
    }
}

ConfigurationEditorProperty.propTypes = {
    readableName: PropTypes.string,
    propertyName: PropTypes.string,
    propertyValue: PropTypes.any,
    editingPropertyName: PropTypes.string,
    dispatch: PropTypes.func.isRequired,
    configuration: PropTypes.object
};

ConfigurationEditorProperty.defaultProps = {
    editing: false
};

function mapEditorPropsToState(state) {
    return {
        currentUser: state.account,
        configuration: state.configuration.editingConfiguration,
        editingPropertyName: state.configuration.editingPropertyName
    };
}

export default connect(mapEditorPropsToState)(ConfigurationEditorProperty);

