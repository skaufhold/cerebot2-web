import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import BsPanel from 'react-bootstrap/lib/Panel';
import BsListGroup from 'react-bootstrap/lib/ListGroup';
import Spinner from '../../elements/Spinner';

import humanize from 'underscore.string/humanize';
import titleize from 'underscore.string/titleize';

import ConfigurationEditorGroup from './ConfigurationEditorGroup';
import ConfigurationEditorProperty from './ConfigurationEditorProperty';


class ConfigurationEditor extends Component {
    render() {
        const { configuration } = this.props;
        if (!configuration) {
            return <Spinner />;
        }

        const properties = Object.keys(configuration)
            .filter(propName => {
                return configuration.hasOwnProperty(propName) && !propName.startsWith('_') && !configuration[propName].hasOwnProperty('_groupName');
            });
        const groups = Object.keys(configuration)
            .filter(propName => {
                return configuration.hasOwnProperty(propName) && !propName.startsWith('_') && configuration[propName].hasOwnProperty('_groupName');
            });
        return (
            <div className="configurationEditor">
                <BsPanel>
                    <BsListGroup striped className="configurationEditor__properties" fill>
                        {properties.map(propName => {
                                const prop = configuration[propName];
                                return (<ConfigurationEditorProperty
                                    key={`${configuration._id}_${propName}`}
                                    readableName={titleize(humanize(propName))}
                                    propertyName={propName}
                                    propertyValue={prop} />);
                            })}
                    </BsListGroup>

                </BsPanel>
                <div className="configurationEditor__groups" fill>
                    {groups.map(propName => {
                        const prop = configuration[propName];
                        return (<ConfigurationEditorGroup key={`${configuration._id}_${propName}`}
                                                          propertyGroup={prop}
                                                          propertyGroupName={propName} />);
                        })}
                </div>
            </div>
        );
    }
}
ConfigurationEditor.propTypes = {
    currentUser: PropTypes.object,
    configuration: PropTypes.object,
    dispatch: PropTypes.func
};

function mapEditorPropsToState(state) {
    return {
        currentUser: state.account,
        configuration: state.configuration.editingConfiguration
    };
}

export default connect(mapEditorPropsToState)(ConfigurationEditor);
