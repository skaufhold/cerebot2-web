import React, { Component, PropTypes } from 'react';
import BsTable from 'react-bootstrap/lib/Table';
import BsButton from 'react-bootstrap/lib/Button';
import { connect } from 'react-redux';
import { createConfiguration, setActiveConfiguration, deleteConfiguration } from '../../actions/configurations';
import objectAssignDeep from 'object-assign-deep';

class ConfigurationList extends Component {
    render() {
        const { configurations, dispatch } = this.props;
        return (
            <div className="configurationList">
                <BsTable striped hover>
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Active</th>
                        </tr>
                    </thead>
                    <tbody>
                    {configurations.map((config) => {
                        return (<tr key={config._id}>
                            <td>{config._meta.name}</td>
                            <td>{config._meta.active ? 'Yes' : 'No' }</td>
                            <td>
                                <BsButton bsSize="xsmall" onClick={() => dispatch(setActiveConfiguration(config._id))}>Make Active</BsButton>
                                <BsButton bsSize="xsmall" onClick={() => this.cloneConfiguration(config)}>Duplicate</BsButton>
                                <BsButton bsSize="xsmall" onClick={() => confirm('Delete this?') ? dispatch(deleteConfiguration(config._id)) : null} bsStyle="danger" disabled={config._meta.active}>Remove</BsButton>
                            </td>
                        </tr>);
                        })}
                    </tbody>
                </BsTable>
            </div>
        );
    }

    cloneConfiguration(config) {
        let newConfig = objectAssignDeep({}, config);
        newConfig._id = undefined;
        newConfig._meta.active = false;
        newConfig._meta.name = config._meta.name + ' (copy)';
        this.props.dispatch(createConfiguration(newConfig));
    }
}

ConfigurationList.propTypes = {
    currentUser: PropTypes.object,
    configurations: PropTypes.array.isRequired,
    dispatch: PropTypes.func
};

function mapStateToProps(state) {
    return {
        currentUser: state.account,
        configurations: state.configuration.available,
        isWaiting: state.configuration.isWaiting
    };
}

export default connect(mapStateToProps)(ConfigurationList);
