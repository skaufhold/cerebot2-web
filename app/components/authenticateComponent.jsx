import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { showFlash } from '../actions/flash';

export function requireAuthentication(Component, permission) {
    class AuthenticateComponent extends React.Component {

        constructor(props) {
            super(props);
        }

        componentWillMount() {
            const { dispatch, isAuthenticated } = this.props;
            if (!isAuthenticated) {
                dispatch(showFlash('error', 'You need to be logged in to do this!'));
                this.context.router.push(`/login?next=${this.props.location.pathname}`);
            } else if (permission && !this.matchPermission()) {
                dispatch(showFlash('error', "You don't have permission to do this!"));
            }
        }

        render() {
            return (
                <Component {...this.props} />
            );
        }

        matchPermission() {
            const { user } = this.props;
            if (permission === 'admin') {
                return user.admin;
            } else if (permission === 'moderator') {
                return user.admin || user.moderator;
            }
        }
    }

    AuthenticateComponent.propTypes = {
        dispatch: PropTypes.func.isRequired,
        isAuthenticated: PropTypes.bool,
        user: PropTypes.object,
        location: PropTypes.object,
        history: PropTypes.object.isRequired
    };

    AuthenticateComponent.contextTypes = {
        router: PropTypes.object.isRequired
    };

    function mapStateToProps(state) {
        return {
            token: state.account.token,
            isAuthenticated: state.account.authenticated,
            user: state.account.user
        };
    }

    return connect(mapStateToProps)(AuthenticateComponent);
}
