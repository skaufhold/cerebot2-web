import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';
import moment from 'moment';

export default class ChatUserRow extends Component {
    render() {
        const { user } = this.props;
        return (
            <tr>
                <td><Link to={`/users/${user.nick}`}>{(user.nick) ? user.nick : 'Unknown'}</Link></td>
                <td>{user.messageCount}</td>
                <td>{user.lastSeen ?
                    moment(user.lastSeen).fromNow() :
                    'Never'}
                </td>
            </tr>
        );
    }
}

ChatUserRow.propTypes = {
    currentUser: PropTypes.object,
    user: PropTypes.object.isRequired,
    dispatch: PropTypes.func
};
