import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { loadUser } from 'actions/chatUsers';
import ChatUserHistory from 'components/chatUser/ChatUserHistory';

import classNames from 'classnames/bind';
import styles from '../../scss/components/_user.scss';

const cx = classNames.bind(styles);

class UserPage extends Component {
    componentDidMount() {
        const { dispatch } = this.props;
        dispatch(loadUser(this.props.params.nick));
    }

    render() {
        const { user } = this.props;
        if (user) {
            return (
                <div className={cx('user_page')}>
                    <section className={cx('user_page__info')}>
                        <h2>{user.nick}</h2>
                        <p>Total lines posted: {user.messageCount}<br/></p>
                    </section>
                    <ChatUserHistory />
                </div>
            );
        }

        return (
            <div className={cx('user_page')}>
                <span className="spinner" />
            </div>
        );
    }
}

UserPage.propTypes = {
    currentUser: PropTypes.object,
    user: PropTypes.object,
    dispatch: PropTypes.func,
    params: PropTypes.object
};

function mapStateToProps(state) {
    return {
        currentUser: state.account,
        user: state.chatUser.user
    };
}

export default connect(mapStateToProps)(UserPage);
