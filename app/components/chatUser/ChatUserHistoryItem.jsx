import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

import ObjectId from 'bson-objectid';
import moment from 'moment';

class ChatUserHistoryItem extends Component {
    render() {
        const { message } = this.props;
        return (
            <tr>
                <td>{moment(ObjectId(message._id).getTimestamp()).format('HH:mm:ss')}</td>
                <td>{message.body}</td>
            </tr>
        );
    }
}

ChatUserHistoryItem.propTypes = {
    currentUser: PropTypes.object,
    user: PropTypes.object.isRequired,
    message: PropTypes.object.isRequired
};

function mapStateToProps(state) {
    return {
        currentUser: state.account,
        user: state.chatUser.user
    };
}

export default connect(mapStateToProps)(ChatUserHistoryItem);
