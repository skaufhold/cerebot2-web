import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import ChatUserRow from './ChatUserRow';
import { loadUserList } from '../../actions/chatUsers';
import { sortAscOrInvert, sortDescOrInvert } from '../../utils/sort';

import BsTable from 'react-bootstrap/lib/Table';
import Pagination from 'react-bootstrap/lib/Pagination';
import Spinner from '../../elements/Spinner';

import classNames from 'classnames/bind';
import styles from '../../scss/components/_user.scss';
const cx = classNames.bind(styles);

export default class ChatUserIndex extends Component {
    componentDidMount() {
        const { dispatch, activePage, itemsPerPage } = this.props;
        dispatch(loadUserList({}, (activePage - 1) * itemsPerPage, itemsPerPage));
    }

    render() {
        const { users, dispatch, itemsPerPage, activePage } = this.props;
        if (!users) {
            return <Spinner />;
        }
        return (
            <div>
                <h1>Users</h1>
                <BsTable className="chatUsers" striped bordered condensed>
                    <thead>
                        <tr>
                            <th>
                                <a className={cx('chatUsers__headerLink', {'chatUsers__headerLink-sorted': this.props.sort.nick})}
                                   href="#"
                                   onClick={() => dispatch(loadUserList({nick: sortAscOrInvert(this.props.sort.nick)}))}>Twitch Username</a>
                            </th>
                            <th>
                                <a className={cx('chatUsers__headerLink', {'chatUsers__headerLink-sorted': this.props.sort.messageCount})}
                                   href="#"
                                   onClick={() => dispatch(loadUserList({messageCount: sortDescOrInvert(this.props.sort.messageCount)}))}>Lines posted</a>
                            </th>
                            <th>
                                <a className={cx('chatUsers__headerLink', {'chatUsers__headerLink-sorted': this.props.sort.lastSeen})}
                                   href="#"
                                   onClick={() => dispatch(loadUserList({lastSeen: sortDescOrInvert(this.props.sort.lastSeen)}))}>Last Seen</a>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        {users.docs.map(u => <ChatUserRow key={u._id} user={u} />)}
                    </tbody>
                </BsTable>
                <Pagination
                    bsSize="small"
                    items={Math.ceil(users.total / itemsPerPage)}
                    activePage={activePage}
                    onSelect={this.selectHistoryPage.bind(this)} />
            </div>
        );
    }

    selectHistoryPage(event, selectEvent) {
    }
}

ChatUserIndex.propTypes = {
    currentUser: PropTypes.object,
    users: PropTypes.object,
    dispatch: PropTypes.func,
    sort: PropTypes.object,
    activePage: PropTypes.number,
    itemsPerPage: PropTypes.number
};

ChatUserIndex.defaultProps = {
    sort: {},
    activePage: 1,
    itemsPerPage: 30
};

function mapStateToProps(state) {
    return {
        currentUser: state.account,
        users: state.chatUser.users,
        sort: state.chatUser.sort
    };
}

export default connect(mapStateToProps)(ChatUserIndex);
