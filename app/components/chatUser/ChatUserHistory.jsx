import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { loadUserHistory } from '../../actions/chatUsers';

import classNames from 'classnames/bind';
import styles from '../../scss/components/_user.scss';
const cx = classNames.bind(styles);

import ChatUserHistoryItem from './ChatUserHistoryItem';
import BsTable from 'react-bootstrap/lib/Table';
import Pagination from 'react-bootstrap/lib/Pagination';
import Spinner from '../../elements/Spinner';

class ChatUserHistory extends Component {
    componentDidMount() {
        const { dispatch, user, itemsPerPage, activePage } = this.props;
        dispatch(loadUserHistory(user.nick, itemsPerPage * (activePage - 1), this.props.itemsPerPage));
    }

    render() {
        const { user, history, activePage, itemsPerPage } = this.props;
        if (!history) {
            return (
                <section className={cx('user_page__history')}>
                    <Spinner />
                </section>
            );
        }
        return (
            <section className={cx('user_page__history')}>
                <BsTable striped bordered condensed>
                    <tbody>{ history.docs.map(message => <ChatUserHistoryItem key={message._id} message={message} user={user} />) }</tbody>
                </BsTable>
                <Pagination
                    bsSize="small"
                    items={Math.ceil(history.total / itemsPerPage)}
                    activePage={activePage}
                    onSelect={this.selectHistoryPage.bind(this)} />
            </section>
        );
    }

    selectHistoryPage(event, selectEvent) {
        this.props.dispatch(loadUserHistory(
            this.props.user.nick,
            this.props.itemsPerPage * (selectEvent.eventKey - 1),
            this.props.itemsPerPage));
    }
}

ChatUserHistory.propTypes = {
    currentUser: PropTypes.object,
    user: PropTypes.object.isRequired,
    history: PropTypes.object,
    dispatch: PropTypes.func,
    activePage: PropTypes.number,
    itemsPerPage: PropTypes.number
};

ChatUserHistory.defaultProps = {
    activePage: 1,
    itemsPerPage: 30
};

function mapStateToProps(state) {
    return {
        currentUser: state.account,
        user: state.chatUser.user,
        history: state.chatUser.history
    };
}

export default connect(mapStateToProps)(ChatUserHistory);
