import React, { PropTypes } from 'react';
import Well from 'react-bootstrap/lib/Well';
import Button from 'react-bootstrap/lib/Button';
import { connect } from 'react-redux';
import Dashboard from './Dashboard';

class Home extends React.Component {
    render() {
        const { isAuthenticated, user } = this.props;
        return (
            <div>
                <Well bsSize="large">
                    <h2>{ isAuthenticated ? `Welcome, ${user.nick}!` : 'Welcome!' }</h2>
                    { isAuthenticated ?
                        <Dashboard /> :
                        <a href="/auth/twitch"><Button bsStyle="primary">Login with Twitch</Button></a> }
                </Well>
            </div>
        );
    }
}

Home.propTypes = {
    isAuthenticated: PropTypes.bool,
    user: PropTypes.object
};

function mapStateToProps(state) {
    return {
        isAuthenticated: state.account.authenticated,
        user: state.account.user
    };
}

export default connect(mapStateToProps)(Home);
