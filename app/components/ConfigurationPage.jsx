import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { loadConfigurations } from 'actions/configurations';

import classNames from 'classnames/bind';
import styles from '../scss/components/_configurations.scss';

import ConfigurationEditor from 'components/configurations/ConfigurationEditor';
import ConfigurationList from 'components/configurations/ConfigurationList';
import Spinner from '../elements/Spinner';

const cx = classNames.bind(styles);

class ConfigurationPage extends Component {
    componentDidMount() {
        const { dispatch } = this.props;
        dispatch(loadConfigurations());
    }

    render() {
        const { dispatch, availableConfigurations } = this.props;

        if (this.props.isWaiting || !availableConfigurations) {
            return (
                <div className={cx('configuratonsPage')}>
                    <Spinner/>
                </div>
            );
        }

        return (
            <div className={cx('configurationsPage')}>
                <section className={cx('configurationsPage__available')}>
                    <h2>Current Configuration</h2>
                    <ConfigurationEditor/>
                </section>
                <section className={cx('configurationsPage__active')}>
                    <h2>Available Configurations</h2>
                    <ConfigurationList/>
                </section>
            </div>
        );
    }
}

ConfigurationPage.propTypes = {
    currentUser: PropTypes.object,
    dispatch: PropTypes.func,
    params: PropTypes.object,
    availableConfigurations: PropTypes.array,
    isWaiting: PropTypes.bool
};

function mapStateToProps(state) {
    return {
        currentUser: state.account,
        availableConfigurations: state.configuration.available,
        isWaiting: state.configuration.isWaiting
    };
}

export default connect(mapStateToProps)(ConfigurationPage);
