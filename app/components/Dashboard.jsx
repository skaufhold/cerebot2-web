import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

export default class Dashboard extends Component {
  render() {
    return (
      <div>Welcome to the Dashboard</div>
    );
  }
}

Dashboard.propTypes = {
  user: PropTypes.object,
  dispatch: PropTypes.func
};

function mapStateToProps(state) {
  return {
    user: state.user
  };
}

export default connect(mapStateToProps)(Dashboard);
