import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { logOut } from '../actions/accounts';
import { selectItem } from '../actions/navigation';
import BsNav from 'react-bootstrap/lib/Nav';
import BsNavItem from 'react-bootstrap/lib/NavItem';
import { LinkContainer } from 'react-router-bootstrap';
import { loadFromServer } from '../utils/urlHelpers';
import '../scss/components/_navigation.scss';

class Navigation extends Component {
    render() {
        const { dispatch } = this.props;
        return (
            <nav>
                <BsNav bsStyle="pills" stacked onSelect={(e)=>dispatch(selectItem(e))}>
                    <LinkContainer to="/">
                        <BsNavItem>cerebot v2</BsNavItem>
                    </LinkContainer>
                    <LinkContainer to="/users">
                        <BsNavItem>Users</BsNavItem>
                    </LinkContainer>
                    <LinkContainer to="/configurations">
                        <BsNavItem>Settings</BsNavItem>
                    </LinkContainer>
                    { this.props.user.authenticated ?
                        (<LinkContainer to="/logout" onClick={()=>dispatch(logOut())}>
                            <BsNavItem>Logout</BsNavItem>
                        </LinkContainer>) :
                        (<LinkContainer to="/auth/twitch" onClick={loadFromServer}>
                            <BsNavItem>Login</BsNavItem>
                        </LinkContainer>)
                    }
                </BsNav>
            </nav>
        );
    }
}

Navigation.propTypes = {
    navigation: PropTypes.object,
    configuration: PropTypes.object,
    user: PropTypes.object,
    dispatch: PropTypes.func.isRequired
};

function mapStateToProps(state) {
    return {
        user: state.account,
        navigation: state.navigation,
        configuration: state.configuration
    };
}

export default connect(mapStateToProps)(Navigation);
