import React, { Component, PropTypes } from 'react';
import Col from 'react-bootstrap/lib/Col';
import Navigation from './Navigation';
import Flash from '../elements/Flash';

import styles from '../scss/main.scss';
import classNames from 'classnames/bind';
const cx = classNames.bind(styles);

/*
 * This component operates as a "Controller-View". It listens for changes in the
 * Store and passes the new data to its children.
 *
 * React provides the kind of composable views we need for the view layer. Close to the top of the nested view hierarchy,
 * a special kind of view listens for events that are broadcast by the stores that it depends on. One could call this a
 * controller-view, as it provides the glue code to get the data from the stores and to pass this data down the chain of its
 * descendants. We might have one of these controller views governing any significant section of the page.
 *
 * When it receives an event from the store, it first requires the new data via the store's public getter methods. It then calls
 * its own setState() or forceUpdate() methods, causing its own render() method and the render() method of all its descendants to run.
 *
 * We often pass the entire state of the store down the chain of views in a single object, allowing different descendants to use
 * what they need. In addition to keeping the controller-like behavior at the top of the hierarchy, and thus keeping our descendant
 */
export default class App extends Component {
    render() {
        return (
            <div>
                <Col componentClass="aside" id={cx('leftSidebar')} xs={12} md={3} lg={2}>
                    <Navigation />
                </Col>
                <Col id={cx('mainContentWrapper')} xs={12} md={9} lg={10}>
                    <Flash />
                    {this.props.children}
                </Col>
            </div>
        );
    }
}

App.propTypes = {
    children: PropTypes.object
};
