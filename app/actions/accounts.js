// Including es6-promise so isomorphic fetch will work
import 'es6-promise';
import fetch from 'isomorphic-fetch';

import * as types from 'constants/actionTypes';

// Note this can be extracted out later
/*
 * Utility function to make AJAX requests using isomorphic fetch.
 * You can also use jquery's $.ajax({}) if you do not want to use the
 * /fetch API.
 * @param Object Data you wish to pass to the server
 * @param String HTTP method, e.g. post, get, put, delete
 * @param String endpoint - defaults to /login
 * @return Promise
 */
function makeAccountRequest(method, data, api = '/login') {
    return fetch(api, {
        method,
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        credentials: 'include',
        body: JSON.stringify(data)
    });
}


// Log In Action Creators
function beginLogin() {
    return {type: types.MANUAL_LOGIN_USER};
}

function loginSuccess(user) {
    return {
        type: types.LOGIN_SUCCESS_USER,
        user
    };
}

function loginError() {
    return {type: types.LOGIN_ERROR_USER};
}

// Sign Up Action Creators
function signUpError() {
    return {type: types.SIGNUP_ERROR_USER};
}

function beginSignUp() {
    return {type: types.SIGNUP_USER};
}

function signUpSuccess(user) {
    return {
        type: types.SIGNUP_SUCCESS_USER,
        user
    };
}

// Log Out Action Creators
function beginLogout() {
    return {type: types.LOGOUT_USER};
}

function logoutSuccess() {
    return dispatch => {
        dispatch({type: types.LOGOUT_SUCCESS_USER});
        dispatch({
            type: types.SHOW_FLASH,
            success: ['Successfully logged out!']
        });
    };
}

function logoutError() {
    return {type: types.LOGOUT_ERROR_USER};
}

export function manualLogin(data) {
    return dispatch => {
        dispatch(beginLogin());

        return makeAccountRequest('post', data, '/login')
            .then(response => {
                if (response.status === 200) {
                    dispatch(loginSuccess(response.json()));
                } else {
                    dispatch(loginError());
                }
            });
    };
}

export function signUp(data) {
    return dispatch => {
        dispatch(beginSignUp());

        return makeAccountRequest('post', data, '/signup')
            .then(response => {
                if (response.status === 200) {
                    dispatch(signUpSuccess(response.json()));
                } else {
                    dispatch(signUpError());
                }
            });
    };
}

export function logOut() {
    return dispatch => {
        dispatch(beginLogout());

        return fetch('/logout', {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            credentials: 'include'
        })
            .then(response => {
                if (response.status === 200) {
                    dispatch(logoutSuccess());
                } else {
                    dispatch(logoutError());
                }
            });
    };
}

