import * as types from '../constants/actionTypes';

export function showFlash(type, message) {
    let action = {
        type: types.SHOW_FLASH
    };
    action[type] = message;
    return action;
}
