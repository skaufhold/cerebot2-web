import 'es6-promise';
import fetch from 'isomorphic-fetch';
import { pathToUrl } from '../utils/urlHelpers';
import * as types from '../constants/actionTypes';
import querystring from 'querystring';

function indexLoadError() {
    return {type: types.USER_INDEX_LOAD_ERROR};
}

function indexLoadDone(users) {
    return {
        type: types.USER_INDEX_LOAD_DONE,
        users
    };
}

function indexLoadBegin(sort) {
    return {
        type: types.USER_INDEX_LOAD,
        sort
    };
}

function showLoadError() {
    return {type: types.USER_SHOW_LOAD_ERROR};
}

function showLoadDone(user) {
    return {
        type: types.USER_SHOW_LOAD_DONE,
        user
    };
}

function showLoadBegin() {
    return {type: types.USER_SHOW_LOAD};
}

function historyLoadBegin() {
    return {type: types.USER_HISTORY_LOAD};
}

function historyLoadError() {
    return {type: types.USER_HISTORY_LOAD_ERROR};
}

function historyLoadDone(history) {
    return {
        type: types.USER_HISTORY_LOAD_DONE,
        history
    };
}

function makeUserRequest(method, data, api='/users') {
    return fetch(pathToUrl(api), {
        method: method,
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        credentials: 'include',
        body: (data) ? JSON.stringify(data) : null
    });
}

export function loadUserList(sort, offset = 0, limit = 30) {
    return dispatch => {
        dispatch(indexLoadBegin(sort));
        let path = '/users';

        let query = {limit, offset};

        if (sort) {
            let sortParam = '';
            for (const field in sort) {
                if (sort.hasOwnProperty(field)) sortParam += field + ';' + sort[field] + ';';
            }
            query.sort = sortParam;
        }

        return makeUserRequest('get', null, path + '?' + querystring.stringify(query))
            .then(response => {
                if (response.status !== 200) {
                    dispatch(indexLoadError());
                } else {
                    return response.json();
                }
            }).then(json => {
                if (json) dispatch(indexLoadDone(json));
            });
    };
}

export function loadUser(nick) {
    return dispatch => {
        dispatch(showLoadBegin());
        return makeUserRequest('get', null, `/users/${nick}`)
            .then(response => {
                if (response.status !== 200) {
                    dispatch(showLoadError());
                } else {
                    return response.json();
                }
            }).then(json => {
                if (json) dispatch(showLoadDone(json));
            });
    };
}
export function loadUserHistory(nick, offset = 0, limit = 0) {
    return dispatch => {
        dispatch(historyLoadBegin());
        return makeUserRequest('get', null, `/users/${nick}/history?offset=${offset}&limit=${limit}`)
            .then(response => {
                if (response.status !== 200) {
                    dispatch(historyLoadError());
                } else {
                    return response.json();
                }
            }).then(json => {
                if (json) dispatch(historyLoadDone(json));
            });
    };
}
