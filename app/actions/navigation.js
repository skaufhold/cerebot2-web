import 'es6-promise';

import * as types from 'constants/actionTypes';

export function selectItem(navItem) {
    return {
        type: types.NAVIGATION_SELECT_ITEM,
        navItem
    };
}
