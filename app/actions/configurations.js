import 'es6-promise';
import fetch from 'isomorphic-fetch';
import { makeApiRequest } from '../utils/urlHelpers';
import * as types from '../constants/actionTypes';
import { unknownError } from '../constants/errors';
import qs from 'qs';

/** Configuration Page **/
function loadError() {
    return {type: types.CONFIGURATIONS_LOAD_ERROR };
}

function loadBegin() {
    return {type: types.CONFIGURATIONS_LOAD};
}

function loadDone(configurations) {
    return {
        type: types.CONFIGURATIONS_LOAD_DONE,
        configurations
    };
}

export function loadConfigurations() {
    return dispatch => {
        dispatch(loadBegin());
        return makeApiRequest('/configurations').then(response => {
            if (response.status !== 200) {
                dispatch(loadError());
            } else {
                return response.json();
            }
        }).then(json => {
            if (json) dispatch(loadDone(json));
        });
    };
}

function beginSetActiveConfiguration(configurationId) {
    return {
        type: types.CONFIGURATION_SET_ACTIVE_BEGIN,
        configurationId
    };
}

function setActiveConfigurationDone(configurationId) {
    return {
        type: types.CONFIGURATION_SET_ACTIVE_DONE,
        configurationId
    };
}

function setActiveConfigurationError(error) {
    return {
        type: types.CONFIGURATION_SET_ACTIVE_ERROR,
        error
    };
}

export function setActiveConfiguration(configurationId) {
    return dispatch => {
        dispatch(beginSetActiveConfiguration(configurationId));

        return makeApiRequest('/configurations/active', {
            method: 'post',
            body: qs.stringify({id: configurationId})
        }).then(response => {
            if (response.status === 204) {
                dispatch(setActiveConfigurationDone(configurationId));
            } else {
                dispatch(setActiveConfigurationError(response.json().message));
            }
        });
    };
}

function beginCreateConfiguration(configuration) {
    return {
        type: types.CONFIGURATION_CREATE_BEGIN,
        configuration
    }
}

function createConfigurationDone(configuration) {
    return {
        type: types.CONFIGURATION_CREATE_DONE,
        configuration
    }
}

function createConfigurationError(error) {
    return {
        type: types.CONFIGURATION_CREATE_ERROR,
        error
    }
}

export function createConfiguration(config) {
    return dispatch => {
        dispatch(beginCreateConfiguration(config));

        return makeApiRequest('/configurations', {
            method: 'put',
            body: config
        }).then(response => {
            response.json().then(json => {
                if (response.status === 200) dispatch(createConfigurationDone(json));
                else dispatch(createConfigurationError(json.error));
            })
        });
    }
}

/** Configuration Editor **/
export function beginEditProperty(propertyName) {
    return {
        type: types.CONFIGURATION_EDIT_BEGIN,
        propertyName
    };
}

function beginUpdateProperty() {
    return {
        type: types.CONFIGURATION_EDIT_SAVE
    };
}

function updatePropertyDone(configuration) {
    return {
        type: types.CONFIGURATION_EDIT_SAVE_DONE,
        configuration
    };
}

function updatePropertyError(error) {
    return {
        type: types.CONFIGURATION_EDIT_SAVE_ERROR,
        error
    };
}

export function cancelEditProperty() {
    return {
        type: types.CONFIGURATION_EDIT_CANCEL
    };
}

export function updateProperty(configurationId, property, value) {
    return dispatch => {
        dispatch(beginUpdateProperty());

        return makeApiRequest('/configurations/' + configurationId, {
            method: 'post',
            body: qs.stringify({property, value})
        }).then(response => {
            response.json().then(json => {
                if (response.status !== 200) {
                    dispatch(updatePropertyError(json.error));
                } else {
                    return dispatch(updatePropertyDone(json));
                }
            }, () => dispatch(updatePropertyError(unknownError)));
        })
    };
}

function beginDeleteConfiguration(configurationId) {
    return {
        type: types.CONFIGURATION_DELETE_BEGIN,
        configurationId
    };
}

function deleteConfigurationDone(configurationId) {
    return {
        type: types.CONFIGURATION_DELETE_DONE,
        configurationId
    };
}

function deleteConfigurationError(error) {
    return {
        type: types.CONFIGURATION_DELETE_ERROR,
        error
    };
}

export function deleteConfiguration(configurationId) {
    return dispatch => {
        dispatch(beginDeleteConfiguration(configurationId));

        return makeApiRequest('/configurations/' + configurationId, {
            method: 'delete'
        }).then(response => {
            if (response.status !== 204) {
                response.json().then(json => {
                    dispatch(deleteConfigurationError(json.error));
                }, () => dispatch(deleteConfigurationError(unknownError)));
            } else {
                return dispatch(deleteConfigurationDone(configurationId));
            }
        })
    };
}
