
import objectAssignDeep from 'object-assign-deep';

export const baseUrl = (process.env.NODE_ENV == 'production') ? '//www.ceredev.eu' : '//localhost:3000';

export function pathToUrl(path) {
    return baseUrl + path;
}

export function loadFromServer(e) {
    window.location.href = e.currentTarget.href;
}

const contentTypes = {
    'string': 'application/x-www-form-urlencoded',
    'object': 'application/json',
    'undefined': undefined
};

export function makeApiRequest(path, options = {}) {
    let opts = objectAssignDeep({
        method: 'get',
        headers: {
            'Accept': 'application/json',
            'Content-Type': contentTypes[typeof options.body]
        },
        credentials: 'include'
    }, options);
    if ((typeof opts.body) == 'object') opts.body = JSON.stringify(opts.body);
    return fetch(pathToUrl(path), opts);
}