
export function sortAscOrInvert(currentSort) {
    if (currentSort === 'asc') return 'desc';
    return 'asc';
}

export function sortDescOrInvert(currentSort) {
    if (currentSort === 'desc') return 'asc';
    return 'desc';
}
